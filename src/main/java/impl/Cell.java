package main.java.impl;

public class Cell {
    private final int row;
    private final int col;
    private int value = 0;
    private final int orignalValue;


    public Cell(int orignalValue, int row, int col) {
        this.row = row;
        this.col = col;
        this.orignalValue = orignalValue;
    }


    public void setValue(int value) {
        this.value = value;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }


    public int getValue() {
        if(value == 0 ){
            return orignalValue;
        }else{
            return value;
        }
    }


    public boolean setValidNumber(int guessNumber, Cell[][] sudoku) {

        int sudokuSize =sudoku[0].length;

        //Check row
        for (int i = 0; i < sudokuSize; i++) {
            if (guessNumber == sudoku[this.getRow()][i].getValue()) {
                return false;
            }
        }

        //check column
        for (int i = 0; i < sudokuSize; i++) {
            if (guessNumber == sudoku[i][this.getCol()].getValue()) {
                return false;
            }
        }

        //check box 3x3
        int rowEnd = sudokuSize;
        int colEnd = sudokuSize;

        if (this.getRow() < 3) {
            rowEnd = 3;
        } else if (this.getRow() < 6) {
            rowEnd = 6;
        }

        if (this.getCol() < 3) {
            colEnd = 3;
        } else if (this.getCol() < 6) {
            colEnd = 6;
        }

        //loop box to check guess
        for (int y = (rowEnd - 3); y < rowEnd; y++) {
            for (int x = (colEnd - 3); x < colEnd; x++) {
                if (guessNumber == sudoku[y][x].getValue()) {
                    return false;
                }
            }
        }

        this.setValue(guessNumber);

        return true;
    }
}
