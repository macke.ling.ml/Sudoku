package main.java.impl;

import main.java.Puzzle;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;



public class PuzzleImpl implements Puzzle {

    private static final int UNSOLVED = 0;
    private static final int SUDOKU_ROW_SIZE = 9;

    // ow/column
    Cell[][] sudoku = new Cell[SUDOKU_ROW_SIZE][SUDOKU_ROW_SIZE];

    private boolean valid = false;
    private boolean solved = false;

    public boolean isSolved() {
        return solved;
    }

    public boolean isValid() {
        return valid;
    }

    public Puzzle solve() {
        //backtracking loop start from top left corner
        if (valid) {
            solved = solveCell(0, 0);
        }

        return this;
    }

    //recursive method
    private boolean solveCell(int row, int col) {

        //Should be solved, start ending recursive loop
        if (row == SUDOKU_ROW_SIZE) {
            return true;
        }

        // Current cell
        Cell cell = sudoku[row][col];

        //next cell
        int nextRow = row;
        int nextCol = col + 1;
        // if last column goto next row
        if (nextCol == SUDOKU_ROW_SIZE) {
            nextRow++;
            nextCol = 0;
        }

        if (cell.getValue() != UNSOLVED) {
            //cell contains value move along
            return solveCell(nextRow, nextCol);

        } else {
            for (int guess : new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}) {
                if (cell.setValidNumber(guess, sudoku)) {

                    if (solveCell(nextRow, nextCol)) {
                        return true;
                    } else {
                        //reset value for backtracking (cell check at line 50)
                        cell.setValue(UNSOLVED);
                    }
                }
            }
        }


        return false;
    }

    public void load(String path) throws IOException {
        valid = true;
        FileReader fr = new FileReader(path);
        BufferedReader br = new BufferedReader(fr);
        String row;
        int rowCount = 0;
        while ((row = br.readLine()) != null) {
            if (rowCount > 9 || row.length() > 9) {
                valid = false;
                return;
            } else {
                for (int i = 0; i < row.length(); i++) {
                    char ch = row.charAt(i);
                    int value = Character.getNumericValue(row.charAt(i));

                    if (ch == ".".charAt(0)) {

                        value = UNSOLVED;

                    } else if (value == -1) {
                        valid = false;
                        return;
                    }
                    sudoku[rowCount][i] = new Cell(value, rowCount, i);
                }
            }

            rowCount++;
        }
    }

    public void save(String path) throws IOException {
        if (isValid()) {
            PrintWriter writer = new PrintWriter(path, "UTF-8");
            for (int row = 0; row < SUDOKU_ROW_SIZE; row++) {
                for (int col = 0; col < SUDOKU_ROW_SIZE; col++) {
                    writer.print(sudoku[row][col].getValue());
                }
                writer.println();
            }
            writer.close();
        }
    }


}


